# 보유기술

[**:leftwards_arrow_with_hook: 메인 목차로 돌아가기 :leftwards_arrow_with_hook:**](./README.md)

## 백엔드

|기술/도구|주요 프로젝트/강의|사용기간|
|-|-|-|
|**Spring Boot**|[픽미업 - 지역 상권 쇼핑몰](./Projects.md#지역-상권-쇼핑몰-픽미업)<br>[DOBDA - 주변 사람들과 의뢰를 주고받는 사이트](./Projects.md#의뢰-공유-사이트-dobda)|<1년|
|**Spring Data JPA**|[픽미업 - 지역 상권 쇼핑몰](./Projects.md#지역-상권-쇼핑몰-픽미업)<br>[DOBDA - 주변 사람들과 의뢰를 주고받는 사이트](./Projects.md#의뢰-공유-사이트-dobda)<br>[JPA 기본편 수강 - 김영한](https://www.inflearn.com/course/ORM-JPA-Basic)|<1년|
|**Spring Security**||<1년|
|**ASP.NET**|[마중물 과제](./Projects.md#마중물-과제)<br>[자동모니터링 시스템 개발](./Projects.md#자동모니터링-시스템-개발-프로젝트)|>1년|
|**Spring MVC**||<1년|
|**Maven**||<1년|
|***Java***||>3년|
|**IntelliJ**||<1년|

## 프론트엔드

|기술/도구|주요 프로젝트/강의|사용기간|
|-|-|-|
|**React**|[픽미업 - 지역 상권 쇼핑몰](./Projects.md#지역-상권-쇼핑몰-픽미업)<br>[DOBDA - 주변 사람들과 의뢰를 주고받는 사이트](./Projects.md#의뢰-공유-사이트-dobda)|<1년|
|**Vue.js**|[개인 홈페이지](./Projects.md#개인-홈페이지)|<1년
|**React-redux**||<1년|
|**HTML, CSS, JS**||<1년|
|**VS Code**||>3년|

## .NET Framework

|기술/도구|주요 프로젝트/강의|사용기간|
|-|-|-|
|**WPF**|[자동모니터링 시스템 개발](./Projects.md#자동모니터링-시스템-개발-프로젝트)|>2년|
|**DevExpress**|[자동모니터링 시스템 개발](./Projects.md#자동모니터링-시스템-개발-프로젝트)|>2년
|**Prism MVVM**||>1년|
|**Winform**||>1년|
|**Visual Studio 2016~2018**||>2년|

## Version Control

|기술/도구|주요 프로젝트/강의|사용기간|
|-|-|-|
|**Git**||>8년|
|**Git-flow**||>3년|
|**Git submodule**|[자동모니터링 시스템 개발](./Projects.md#자동모니터링-시스템-개발-프로젝트)|>1년|
|**GitLab**|[링크](https://gitlab.com/kakaruu)<br>|>3년|
|**Fork (GUI tool)**|[링크](https://git-fork.com/)|>3년|

## DevOps

|기술/도구|주요 프로젝트/강의|사용기간|
|-|-|-|
|**Docker**|[docker-sshd](./Projects.md#docker-sshd)<br>[자동모니터링 시스템 개발](./Projects.md#자동모니터링-시스템-개발-프로젝트)|>2년|
|**GitLab CI/CD**|[자동모니터링 시스템 개발](./Projects.md#자동모니터링-시스템-개발-프로젝트)<br>[픽미업 - 지역 상권 쇼핑몰](./Projects.md#지역-상권-쇼핑몰-픽미업)|>1년|
|**MySQL**|[픽미업 - 지역 상권 쇼핑몰](./Projects.md#지역-상권-쇼핑몰-픽미업)<br>[DOBDA - 주변 사람들과 의뢰를 주고받는 사이트](./Projects.md#의뢰-공유-사이트-dobda)|<1년|
|**MariaDB**|[자동모니터링 시스템 개발](./Projects.md#자동모니터링-시스템-개발-프로젝트)|>2년|
|**Ubuntu**||>2년|
|**Routing**|[자동모니터링 시스템 개발](./Projects.md#자동모니터링-시스템-개발-프로젝트)|>3년|
|**Naver Cloud<br>Platform**|[픽미업 - 지역 상권 쇼핑몰](./Projects.md#지역-상권-쇼핑몰-픽미업)<br>[DOBDA - 주변 사람들과 의뢰를 주고받는 사이트](./Projects.md#의뢰-공유-사이트-dobda)|<1년|

## Else

|기술/도구|주요 프로젝트/강의|사용기간|
|-|-|-|
|**MQTT**|[GRAP](./Projects.md#grap)<br>[석사 과정 졸업 프로젝트](./Projects.md#석사-과정-졸업-프로젝트-저널-게재)<br>[자동모니터링 시스템 개발](./Projects.md#자동모니터링-시스템-개발-프로젝트)|>6년|
|**일본어**|[자동모니터링 시스템 개발](./Projects.md#자동모니터링-시스템-개발-프로젝트)|>2년|
|**Axis IP Camera**|[자동모니터링 시스템 개발](./Projects.md#자동모니터링-시스템-개발-프로젝트)|>1년|
|**Android**|[GRAP](./Projects.md#grap)|>2년|
